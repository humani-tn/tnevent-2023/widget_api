package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/pprof"
	"os"
	"widget-api/internal/rbmq"
	"widget-api/internal/streamers"
	"widget-api/internal/webserver"

	"github.com/joho/godotenv"
)

func main() {
	// Load environment variables from .env when in development
	err := godotenv.Load(".env")
	if err != nil && os.Getenv("MODE") != "prod" {
		log.Panicf("Error loading .env file: %s\nIf this is in production, don't forget MODE=prod !", err)
	}

	rbmq.Startup()

	s := streamers.GetStreamers()

	if os.Getenv("DEBUG") == "true" {
		log.Println("Streamers:")
		for _, streamer := range s {
			fmt.Printf("  - %s: %s\n", streamer.TwitchID, streamer.Name)
		}
	}

	// Start the metrics server and make routes manually
	mux := http.NewServeMux()
	mux.HandleFunc("/debug/pprof/", pprof.Index)
	mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
	mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
	mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
	mux.Handle("/debug/pprof/goroutine", pprof.Handler("goroutine"))
	mux.Handle("/debug/pprof/heap", pprof.Handler("heap"))
	mux.Handle("/debug/pprof/threadcreate", pprof.Handler("threadcreate"))
	mux.Handle("/debug/pprof/block", pprof.Handler("block"))

	// Add a health check route
	mux.HandleFunc("/health", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	// Start the metrics server on port 2112
	go http.ListenAndServe(":2112", mux)

	webserver.Startup(s)
}
