package rbmq

import (
	"log"
	"os"
	"time"
)

func connectionWatchdog() {
	// Ticker that checks every seconds the connection status
	t := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-t.C:
			// If the connection is closed, try to reconnect
			if connection == nil || channel == nil {
				if os.Getenv("DEBUG") == "true" {
					log.Println("Trying to reconnect to RabbitMQ...")
				}
				connection, channel = connect()
			} else if connection.IsClosed() {
				if os.Getenv("DEBUG") == "true" {
					log.Println("Trying to reconnect to RabbitMQ...")
				}
				connection, channel = connect()
			}
		}
	}
}
