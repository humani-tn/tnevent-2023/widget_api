package rbmq

import (
	"fmt"
	"log"
	"os"

	amqp "github.com/rabbitmq/amqp091-go"
)

var channel *amqp.Channel
var connection *amqp.Connection

func Startup() {
	conn, ch := connect()
	channel = ch
	connection = conn

	go connectionWatchdog()
}

func connect() (conn *amqp.Connection, ch *amqp.Channel) {
	var err error
	connectionString := os.Getenv("RBMQ_CONNECTION_STRING")
	conn, err = amqp.Dial(connectionString)
	if err != nil {
		if os.Getenv("DEBUG") == "true" {
			log.Printf("Failed to connect to RabbitMQ: %s\n", err)
		}
		return
	}

	ch, err = conn.Channel()
	if err != nil {
		if os.Getenv("DEBUG") == "true" {
			log.Printf("Failed to connect to RabbitMQ: %s\n", err)
		}
		return
	}

	if os.Getenv("DEBUG") == "true" {
		log.Println("Connected to RabbitMQ")
	}

	return
}

func GetChannel() (*amqp.Channel, error) {
	if channel == nil {
		return nil, fmt.Errorf("channel not initialized")
	}
	return channel, nil
}
