package webserver

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
	"widget-api/internal/rbmq"
	"widget-api/internal/streamers"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	amqp "github.com/rabbitmq/amqp091-go"
)

var counters = make(map[string]int)

func Startup(s []*streamers.Streamer) {
	frontendURL := os.Getenv("FRONTEND_URL")
	// Initialize gin server
	engine := gin.Default()
	engine.Use(cors.New(cors.Config{
		AllowOrigins: []string{frontendURL},
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders: []string{"Origin", "Content-Type", "Authorization"},
	}))

	routeHandling(engine, s)

	// Update streamers list every 30 minutes
	go func() {
		t := time.NewTicker(30 * time.Minute)
		for {
			select {
			case <-t.C:
				if os.Getenv("DEBUG") == "true" {
					log.Printf("Updating streamers list.")
				}
				s = streamers.GetStreamers()
				routeHandling(engine, s)
			}
		}
	}()

	// Start the server
	engine.Run()
}

func routeHandling(engine *gin.Engine, s []*streamers.Streamer) {
	redir := os.Getenv("REDIR_URL")
	// Add the route of all streamers if it doesn't exist
	for _, streamer := range s {
		// Format TwitchID to lowercase and replace spaces with underscores
		streamRoute := streamer.FormatTwitchID()

		found := false
		for _, route := range engine.Routes() {
			if route.Path == fmt.Sprintf("/view/%s", streamRoute) {
				found = true
			}
		}
		if !found {
			engine.GET(fmt.Sprintf("/view/%s", streamRoute), func(c *gin.Context) {
				c.JSON(200, gin.H{
					"clicked": counters[streamRoute],
				})
			})
			engine.GET(fmt.Sprintf("/redir/%s", streamRoute), func(c *gin.Context) {
				// Store update state
				updated := false
				// Check already clicked cookie
				_, err := c.Cookie("clicked")
				if err != nil {
					// Cookie not found, set it
					c.SetCookie("clicked", "true", 60*60*24*30, fmt.Sprintf("/redir/%s", streamRoute), "", false, true)
					// Increment counter
					counters[streamRoute]++
					updated = true
				}
				c.Redirect(http.StatusMovedPermanently, redir)

				if updated {
					// Prepare body
					data := map[string]interface{}{
						"message": map[string]string{
							"clicks": fmt.Sprintf("%d", counters[streamRoute]),
						},
						"topic": "/widget/" + streamRoute,
					}
					body, err := json.Marshal(data)
					if err != nil {
						log.Printf("Failed to marshal message: %s\n", err)
						return
					}

					// Send update to RabbitMQ
					ch, err := rbmq.GetChannel()
					if err != nil {
						if os.Getenv("DEBUG") == "true" {
							log.Printf("Failed to get channel: %s\n", err)
						}
						return
					}

					ch.PublishWithContext(
						context.Background(),
						os.Getenv("RBMQ_EXCHANGE_NAME"),
						"",
						false,
						false,
						amqp.Publishing{
							ContentType: "text/plain",
							Body:        []byte(body),
						},
					)
				}
			})
		}
	}
}
