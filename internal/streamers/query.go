package streamers

import (
	"encoding/json"
	"io"
	"net/http"
	"strings"

	"os"
)

type Streamer struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	TwitchID    string `json:"twitch_id"`
	ID          int    `json:"id"`
	ImageFile   string `json:"image_file"`
	// Queue       amqp.Queue `json:"queue"`
}

func (s *Streamer) FormatTwitchID() string {
	// Replace spaces with underscores
	twitchID := s.TwitchID
	twitchID = strings.ReplaceAll(twitchID, " ", "_")
	// Convert to lowercase
	twitchID = strings.ToLower(twitchID)
	return twitchID
}

func GetStreamers() []*Streamer {
	URL := os.Getenv("API_STREAMERS")

	// Query the API
	req, err := http.NewRequest("GET", URL, nil)
	if err != nil {
		panic(err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}

	// Parse the response here
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	// Return the streamers
	var streamers []*Streamer

	json.Unmarshal(body, &streamers)
	return streamers
}
