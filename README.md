
# TN Event - Widget API

Ce service sert à donner la possibilité au streamer de pouvoir compter le nombre de personnes (uniques) qui ont clické sur leur lien de donation.

TODO: lien vers l'api

## Dépendance

 - github.com/joho/godotenv v1.4.0 : Gestion de l'environnement en dev
 - github.com/rabbitmq/amqp091-go v1.5.0 : Gestion du RabbitMQ

## Environnement 

- `RBMQ_CONNECTION_STRING` : Permet la connection au service RabbitMQ
- `RBMQ_EXCHANGE_NAME` : Nom de l'échange où transite les données
- `API_STREAMERS` : Lien qui retourne une liste de streamers
- `FRONTEND_URL` : URL du front pour le CORS si on souhaite récupérer a la main sur /view/:id
- `DEBUG` : Affiche quelques messages dans les logs
- `DEBUG_EXTENDED` : Affiche absolument tout
- `MODE` : Détermine le mode de l'application `prod` ou `dev`
- `REDIR_URL` : Lien vers la cagnotte

## Lancement en dev

Il faut d'abord s'assurer d'avoir un .env **complet** et [Go](https://go.dev/) d'installer.

```
git clone **URL**
cd widget_api
go run cmd/server/main.go
```

## Déploiement (prod)

TODO

## Routes 

- `/view/:id` : Permet de lire le nombre de clicks pour l'identifiant. (en théorie pas nécessaire)
- `/redir/:id` : Redirige, ajoute +1 au compteur de l'identifiant et envoie sur RabbitMQ les données.

## Status

Le service ouvre un port de metrics qui doit rester **fermer** (ou sous mot de passe) en production qui est le port `2112`. Il permet de vérifier le status de l'application sur `/health` et de vérifier la consommation du service sur `/debug/pprof`